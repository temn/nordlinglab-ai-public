Deep Learning Tutorial
===


In the following is a Deep Learning Tutorial using Docker. The tutorial has three parts, namely:

1. **Docker and VS Code Installation Guide**  
    This chapter guides you through the installation process and can be skipped if already done. Docker is necessary in order to run and share the code we develop in this tutorial. Here is also a guide for VS Code installation (VSC), which is just a text editor but with optional functionalities that includes Docker commands, python autocompletion and more.

2. **Docker Tutorial**  
    This chapter is intended to make every inexperienced user learn the basics of Docker. Here we will guide through the download and running of an image. The image is the basis for the tutorial we use later on. It includes all packages necessary, such as TensorFlow (TS) and Keras.  

3. **Deep Learning using Python**  
    This chapter will teach the basics of Python and will guide the user step by step towards complex simulations. At the end the user will understand and be able to run deep learning algorithms.


# 1. Docker and VS Code Installation Guide #

Through the following links you can find the tutorials guiding you through the installation process for Docker and VS Code. Docker is a prerequisite since the Deep Learning Tutorial is based on it but VS Code is optional, if you need a texteditor:

   1. [Docker Installation](./Docker_installation.md) (Required)

   2. [VS Code Installation and Docker Integration](./VSCode_installation.md) (Optional)

# 2. Docker Tutorial #

The following Docker tutorial will teach you how to use the Docker image for the Deep Learning Tutorial, as well as two other exercises:

[Docker Tutorial](./Docker_tutorial.md)

# 3. Deep Learning Using Python (TF and Keras) #

By using the Dockerfiles provided in the docker tutorial above, you will be able to do the excercises in the python/ Tensorflow tutorial provided as a presentation in PDF format below. If you have macOS, make sure Docker is running beforehand. 

[**Dropbox Link**](https://www.dropbox.com/s/bjwuswei0py2x3q/Nordlinglab_Python_tutorial.pdf?dl=0) to Python and Tensorflow tutorial.
# README #

This repository if for sharing information related to Artificial Intelligence.

## Presentation Schedule ##

The presentation slides are added as a link under the title of the presentation. The presentation files are shared in this [link](https://www.dropbox.com/sh/c03nwvj8httjbco/AABKvc3ivFj3Ne3roMJbLjmZa?dl=0).

1. 180510 Prof. Nordling - [An introduction to deep learning and the concepts of artificial neural networks](https://www.dropbox.com/s/g3hkrersi6quw6p/Nordling2018_Deep_learning_BME_180510_copy.pdf?dl=0)
1. 180524 Mark Liu 
1. 180607 Jose - [ANN_MNIST](https://www.dropbox.com/s/8zs7knkkto793y6/ANN_MNIST.pptx?dl=0)
1. 180621 Paul - [Docker & deep learning](https://www.dropbox.com/s/2pjw2lk2ug9age7/Paul_docker_deep_learning.pptx?dl=0)
1. 180918 Oliver - [Computing workshop](https://www.dropbox.com/s/id9x8e4kck87jl9/AI_computing_workshop.pptx?dl=0)
1. 181003 Heng-Ying - [IEEE_SUMMER_WORKSHOP](https://www.dropbox.com/s/hrjmw6nmbb70av6/Heng_Ying_IEEE_SUMMER_CAMP.pptx?dl=0)
1. 181017 Hung - [Binarized neural network in emerging memory](https://www.dropbox.com/s/ipubr1unoil3xit/20181017_AI%20Meeting_HUNG.pptx?dl=0)
1. 181031 Hoang-Hiep Le - [AISND TAIWANIA NCHC](https://www.dropbox.com/s/6inz643m6kfudk8/AISND%20TAIWANIA%20NCHC%20-%20BI-WEEKLY%20JOINT%20AI%20MEETING%20AGENDA%2020181030.pptx?dl=0)
1. 181114 Paul - [Google AI Camp](https://www.dropbox.com/s/4s0ei5aoptympj0/Paul_Google_AI_Camp.pptx?dl=0)
1. 181128 Tim 
1. 181212 Dr. Robert Po-Yuan Chen - [Introduction to Hodgkin-Huxley model of action potential](https://www.dropbox.com/s/3845pvldvtwywxj/HodgkinHuxley_Dr-Robert_Po-Yuan_Chen.pdf?dl=0)
1. 181226 Dr. Po-Hsuan (Cameron) Chen - How to Develop Machine Learning Models for  Healthcare - (Google AI)@​成大資訊系舊館4263階梯教室 
1. 190102 [Hands-on introduction to Deep Learning in Tensorflow](./deep_learning_tutorial.md) Part 1: Installation and Usage of Docker (Workshop)
1. 190109 [Hands-on introduction to Deep Learning in Tensorflow](./deep_learning_tutorial.md) Part 2: Docker Usage, Introduction to Deep Learning (Workshop)
1. 190321 Hoang-Hiep Le - [Hidden Markov Model in Speech Recognition An Introduction](https://www.dropbox.com/s/2ibiuhet7d0d3g3/Hidden%20Markov%20Model%20in%20Speech%20Recognition%20An%20Introduction.pptx?dl=0)
1. 190503 Jose - [Transfer learning](https://www.dropbox.com/s/ryrmc8l8xms5xjl/Transfer_learning_InceptionResnetv2.pptx?dl=0)
1. 190517 Prof. Lu - [Dagstuhl Seminar](https://www.dropbox.com/s/nhn8tumilo09trb/20190516%20Dagstuhl%20Seminar%20Highlights.pptx?dl=0)
1. 190523 Prof. Nordling - [Current and future success stories of AI](https://drive.google.com/open?id=1DvUkAn9Z-77pKSa6ONJYHVoJ1SQgBsFL)

## Email list ##

If you are interested in joining the AI meetings, please, sign up to the email list
[http://cpsrv29.misshosting.com/mailman/listinfo/ai_nordlinglab.org](http://cpsrv29.misshosting.com/mailman/listinfo/ai_nordlinglab.org)


## Links

Useful links.

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo) so that you can contribute to this README.

### Overviews of AI

* [Nice overview of AI by Fujitsu CTO Reger Joseph](https://www.youtube.com/watch?v=Iura8fLPrXQ)

### Introductions to Deep learning

* [TensorFlow and Keras high level intro](https://www.youtube.com/watch?v=5DknTFbcGVM)
* Nice intro to doing Deep learning using TensorFlow [part 1](https://www.youtube.com/watch?v=u4alGiomYP4) [part 2](https://www.youtube.com/watch?v=fTUwdXUFfI8)
* Brandon Rohrer's non-mathematical [Deep learning demystified](https://www.youtube.com/watch?v=Q9Z20HCPnww)
* [Nice Machine Learning Recipes with Josh Gordon](https://www.youtube.com/playlist?list=PLOU2XLYxmsIIuiBfYad6rFYQU_jL2ryal)
* [Blog providing insight into some neural networks](http://colah.github.io/)
* [Simple graphical intro to classification using neural networks](https://www.youtube.com/watch?v=BR9h47Jtqyw)
* [Simple graphical intro of the idea behind object recognition in images using convolutional neural networks](https://www.youtube.com/watch?v=2-Ol7ZB0MmU)
* 3blue1brown Nice visualisation of [ANN - "But what *is* a Neural Network? | Chapter 1"](https://youtu.be/aircAruvnKk), [gradient descent - "Gradient descent, how neural networks learn | Chapter 2"](https://youtu.be/IHZwWFHWa-w), [backpropagation - "What is backpropagation really doing? | Chapter 3"](https://youtu.be/Ilg3gGewQ5U), [calculus of backpropagation and the chain rule - "Backpropagation calculus | Appendix to deep learning chapter 3"](https://youtu.be/tIeHLnjs5U8) based on the MINST example in [Neural Networks and Deep Learning](http://neuralnetworksanddeeplearning.com/) by Michael Nielsen.
* [Quora - What's the most effective way to get started with deep learning?](https://www.quora.com/Whats-the-most-effective-way-to-get-started-with-deep-learning) contains many links to good sources.
* Brandon Rohrer's [How Convolutional Neural Networks work](https://www.youtube.com/watch?v=FmpDIaiMIeA)
* Slides on [Image translation with GANs](https://docs.google.com/presentation/d/1uYSM7hR8H6aNv6hGkzfS05ojQiuVOgnq5ASLpGy8IGk/edit?usp=sharing)
* A biased introduction to Machine learning (you will later understand why it is biased) that mainly show cases cool Google products by Jason Mayes [Jason's Machine Learning 101](https://docs.google.com/presentation/d/1kSuQyW5DTnkVaZEjGYCkfOxvzCqGEFzWBy4e9Uedd9k/edit?usp=sharing)

*Ethics, biases, and human safety related talks that are compulsory to watch to avoid these problems*

* [Margaret Mitchell's TED talk "How we can build AI to help humans, not hurt us"](https://www.ted.com/talks/margaret_mitchell_how_we_can_build_ai_to_help_humans_not_hurt_us) Mitchell says: "All that we see now is a snapshot in the evolution of artificial intelligence." "If we want AI to evolve in a way that helps humans, then we need to define the goals and strategies that enable that path now."
* [Joy Buolamwini's TED talk "How I'm fighting bias in algorithms"](https://www.ted.com/talks/joy_buolamwini_how_i_m_fighting_bias_in_algorithms) This is an an eye-opening talk on biases and the need for accountability and understanding of why a decision was made.

### Recommended courses for Deep learning

* Tensorflow for Deep Learning Research [Stanford CS 20SI](http://cs20si.stanford.edu/), [Lecture notes](https://web.stanford.edu/class/cs20si/syllabus.html), [Code](https://github.com/chiphuyen/tf-stanford-tutorials)
* Convolutional Neural Networks for Visual Recognition [Stanford CS 231N](http://cs231n.stanford.edu/), [Lecture notes](http://cs231n.stanford.edu/syllabus.html), [Code](http://cs231n.github.io/)
* [fast.ai's 7 week course, Practical Deep Learning For Coders, Part 1 (Free, good reviews)](http://course.fast.ai/index.html)
* [fast.ai's 7 week course, Cutting Edge Deep Learning For Coders, Part 2 (Free, good reviews)](http://course.fast.ai/part2.html)
* [Coursera Deep learning Specialisation](https://www.coursera.org/specializations/deep-learning#courses)
* Deep Learning at NYU [2017 by Yann LeCun, only slides available](https://cilvr.nyu.edu/doku.php?id=deeplearning2017:schedule), [2015 by Yann LeCun, slides and links to other sources (note that videos of lectures have been removed)](https://cilvr.nyu.edu/doku.php?id=deeplearning2015%3Aschedule), [2017 Homeworks](https://cilvr.nyu.edu/doku.php?id=deeplearning2017:homework). 
* [Deep Learning Summer School in Montreal (Video lectures by many AI scientists)](http://videolectures.net/deeplearning2015_montreal/).
* [Graduate Summer School: Deep Learning, Feature Learning (Schedule) - IPAM (Lecture slides as PDFs)](http://www.ipam.ucla.edu/programs/summer-schools/graduate-summer-school-deep-learning-feature-learning/?tab=schedule)
* [edX Artificial Intelligence at Columbia University](https://www.edx.org/course/artificial-intelligence-ai-columbiax-csmm-101x-2)
* [Microsoft Professional Program for Artificial Intelligence track](https://academy.microsoft.com/en-us/tracks/artificial-intelligence/), 10 courses, Microsoft Professional Program certificate can be issued. "Each course runs for three months and starts at the beginning of a quarter. January—March, April—June, July—September, and October —December. The capstone runs for four weeks at the beginning of each quarter: January, April, July, October." See each course at [EdX](https://www.edx.org/) for schedule details.


### Recommended books

* [Neural Networks and Deep Learning](http://neuralnetworksanddeeplearning.com/) by Michael Nielsen, which also contains exercise code [for Python 2.6/2.7](https://github.com/mnielsen/neural-networks-and-deep-learning) and [Python 3](https://github.com/MichalDanielDobrzanski/DeepLearningPython35) using Theano. Cite as Michael A. Nielsen, Neural Networks and Deep Learning, Determination Press, 2015. Licensed under CC BY non-commercial 3.0 no PDF/print available.
* [Deep learning](http://www.deeplearningbook.org/) by Ian Goodfellow, Yoshua Bengio, and Aaron Courville 2016, which also contains lecture slides and some exercises. (Oliver reads)

### Recommended tutorials

* [TensorFlow Tutorials](https://www.tensorflow.org/tutorials/).
* [Deeplearning.net Theano tutorials](http://deeplearning.net/tutorial/)

### Illustrations, visualisations, mindmaps

* [A mostly complete chart of Neural Networks](http://www.asimovinstitute.org/neural-network-zoo/)
* [Jose R. Gorchs Machine learning A to Z mindmap](http://ibb.co/f2XnV6)
* [CNN illustration](https://harishnarayanan.org/images/writing/artistic-style-transfer/conv-layer.gif)

### Recommended blogs

* [Christopher Olah's blog](http://colah.github.io/) focused on explaining things clearly with nice visual illustrations
* [Michael A. Nielsen](http://michaelnielsen.org/) ideas and tools that help people think and create

### Deep learning libraries

* [TensorFlow](https://www.tensorflow.org/), Python, Apache 2.0 license 
* [Torch](http://torch.ch/), Lua, Own open source license
* [Theano](https://github.com/Theano), Python, Own open source license, "University of Montreal’s MILA are terminating Theano development with the 1.0 release."
* [Caffe](http://caffe.berkeleyvision.org/), A deep learning framework written in C++ with python interface and [OpenCL version](https://github.com/BVLC/caffe/tree/opencl), BSD license.
* [Neurala](https://www.neurala.com/technology/), Commercial, built for low cost hardware. 
* [PaddlePaddle](https://github.com/PaddlePaddle) cross-platform distributed deep learning framework, Python, Apache 2.0 license.
* [Apache MxNet](https://github.com/apache/incubator-mxnet) multiple GPUs multiple programming languages, Apache 2.0 license.
* Multiple GPUs in Tensorflow and Keras (via [Horovod](https://github.com/uber/horovod)) 
* Multiple GPUs in [CNTK](https://docs.microsoft.com/en-us/cognitive-toolkit/Multiple-GPUs-and-machines), Freeware (including commercial use) or non-commercial open source under CNTK licenses
* Multiple GPUs and Dynamical networks in [PyTorch](http://pytorch.org/tutorials/beginner/former_torchies/parallelism_tutorial.html) (new research tool by Facebook research worth checking out)
* [Caffe2](https://caffe2.ai/) a New Lightweight, Modular, and Scalable cross-platform Deep Learning Framework, includes Raspberian and Android with Python and C++ interface, Apache License 2.0.
* [PlaidML](https://github.com/plaidml/plaidml) a new framework for making deep learning work everywhere by [Vertex.ai](http://vertex.ai), which supports OpenCL, AGPLv3.
* [Tensor Comprehensions](https://research.fb.com/announcing-tensor-comprehensions/) a C++ library and mathematical language for specification of models and Just-In-Time compilation automatically and on-demand.

For starting to do Deep Learning using a single GTX 1080Ti GPU or NCHC DGx/DHx TensorFlow and Keras are recommended. For research on network models, including dynamic networks, PyTorch is recommended. For training on multiple GPUs or over a network CNTK is recommended. For IoT and mobile deployment Caffe2, which has support for conversion of models made in Torch, PaddlePaddle, or MxNet is recommended. See the [Wikipedia comparison of libraries](https://en.wikipedia.org/wiki/Comparison_of_deep_learning_software).


### Comparisons of Deep learning libraries

* [Reinders' summary of Intel's efforts to optimise deep learning on CPUs claiming almost GPU performance](https://www.nextplatform.com/2017/10/13/new-optimizations-improve-deep-learning-frameworks-cpus/). We should test running the learning on the Intel Xeon Phi multi-core card with the described optimisations.
* [A collection of best practices for optimizing TensorFlow code](https://www.tensorflow.org/performance/performance_guide)
* [Quora discussion with short points](https://www.quora.com/Is-TensorFlow-better-than-other-leading-libraries-such-as-Torch-Theano)
* [insideHPC machine learning comparison of libraries](https://www.dropbox.com/s/fbta3apkv4ty7fv/Harlen2017_insideHPC_Launch_Machine_Learning_Startup.pdf?dl=0)

### Fooling ANNs

* [Make small changes to fool object recognition](http://www.popsci.com/byzantine-science-deceiving-artificial-intelligence)
* [High confidence predictions for unrecognisable images](http://www.evolvingai.org/fooling)

### Learning methods ###

1. [Boltzmann learning](https://en.wikibooks.org/wiki/Artificial_Neural_Networks/Boltzmann_Learning) - a statistical learning method for ANNs by Geoffrey Hinton and Terry Sejnowski.
1. [Federated learning](https://research.googleblog.com/2017/04/federated-learning-collaborative.html) - distributed training of ANNs without centralising data in a privacy preserving manner. 
1. Concept networks and hierarchical deep reinforcement learning [blog showing 45x speedup](https://bons.ai/blog/rl-benchmark), [paper](http://bns.ai/drl_paper).
1. Generative Adversarial Network(GAN).
1. Hinton's capsule networks and "dynamic routing between capsules" optimisation method, see [intuition](https://medium.com/@pechyonkin/understanding-hintons-capsule-networks-part-i-intuition-b4b559d1159b), [more details](https://medium.com/@pechyonkin/understanding-hintons-capsule-networks-part-ii-how-capsules-work-153b6ade9f66), [original paper](http://www.cs.toronto.edu/~fritz/absps/transauto6.pdf)
1. [Google is using an RNN to train a child ANN](https://research.googleblog.com/2017/05/using-machine-learning-to-explore.html) video presented by [Coldfusion](https://youtu.be/YNLC0wJSHxI), see [Zoph2017](https://arxiv.org/abs/1611.01578) and [Real2017](https://arxiv.org/abs/1703.01041). This decreases the human skill needed and time to train an ANN, so it has high potential for exponential improvements.

### AI applications

* [Text to Image](https://www.digitaltrends.com/cool-tech/ai-generates-images-based-on-text/) [Article](https://arxiv.org/pdf/1612.03242v1.pdf) 
* [Space Invaders](https://www.youtube.com/watch?v=Qvco7ufsX_0)
* [Transferring style from famous paintings](https://deepart.io )
* [Text Description](https://github.com/tensorflow/models/tree/master/im2txt), Torch, Python, [Article](https://arxiv.org/abs/1609.06647), [Public Data under CC-BY 4.0](http://mscoco.org/), [Code](https://github.com/karpathy/neuraltalk2)
* [Energy Price Forecast](https://github.com/tgjeon/TensorFlow-Tutorials-for-Time-Series)
* [Predict-Stock-Prices](https://github.com/llSourcell/How-to-Predict-Stock-Prices-Easily-Demo)
* [Large Pose 3D Face Reconstruction from a Single Image via Direct Volumetric CNN Regression](http://aaronsplace.co.uk/papers/jackson2017recon/), Torch, Python, [Article](https://arxiv.org/abs/1703.07834), [Code under MIT license](https://github.com/AaronJackson/vrn)
* Can an AI get into Tokyo university [TED talk April 2017](https://www.ted.com/talks/noriko_arai_can_a_robot_pass_a_university_entrance_exam), [Todai project website](http://21robot.org/?lang=english)
* [Generative design using AI in Autodesk](https://www.youtube.com/watch?v=5drrN2CxDzc)
* [Object detection using MobileNets and OpenCV](https://www.pyimagesearch.com/2017/09/11/object-detection-with-deep-learning-and-opencv/)
* Object detection research tool by Facebook called [Detectron](https://github.com/facebookresearch/Detectron) written in Caffe 2, see [this image](https://user-images.githubusercontent.com/121322/36002551-6f43188c-0cdf-11e8-8791-1fece79d2ddf.png).

### Top 100 AI startups

List by [CBinsights](https://cbi-blog.s3.amazonaws.com/blog/wp-content/uploads/2017/01/AI_100_market_map_2017-NEW.png)

1. [Dispatch](http://dispatch.ai/how-it-works/) "A platform for local delivery powered by a fleet of autonomous vehicles designed for sidewalks and pedestrian spaces."
1. [MindMeld](https://www.mindmeld.com/about) "A platform that makes it possible for companies to create intelligent conversational interfaces for any app or device."

### Training in games, websites, etc.

* [A software platform for measuring and training an AI’s general intelligence across games, websites and other applications](https://blog.openai.com/universe/)

### AI future perspectives

* [Use of AI to accelerate science (Article in Science)](http://cacs.usc.edu/education/cs653/Gil-AIdiscovery-Science14.pdf).

### Hardware for Deep learning

* [Instructions for building a computer for deep learning for app. 2k USD](https://medium.com/@vivek.yadav/deep-learning-machine-first-build-experience-d04abf198831)
* [Balrog - 4 GTX 1080Ti Deep Learning build with ASUS X99-E WS motherboard](https://www.tooploox.com/blog/deep-learning-with-gpu)
* [Slavv's blog on building Deep Learning computer recommending GTX 1080Ti and AMD Ryzen Threadripper 1950X (64 lanes) with GIGABYTE X399 AORUS Gaming 7](https://blog.slavv.com/picking-a-gpu-for-deep-learning-3d4795c273b9). "Training a single network on several video cards is slowly but surely gaining traction. Nowadays, there are easy to use approaches to this for Tensorflow and Keras (via [Horovod](https://github.com/uber/horovod)), [CNTK](https://docs.microsoft.com/en-us/cognitive-toolkit/Multiple-GPUs-and-machines) and [PyTorch](http://pytorch.org/tutorials/beginner/former_torchies/parallelism_tutorial.html)."
* [Tim Dettmers' blog on building Deep Learning computer recommending GTX 1080Ti](http://timdettmers.com/2017/04/09/which-gpu-for-deep-learning/). "The only deep learning library which currently implements efficient algorithms across GPUs and across computers is CNTK which uses Microsoft’s special parallelization algorithms of 1-bit quantization (efficient) and block momentum (very efficient). With CNTK and a cluster of 96 GPUs you can expect a new linear speed of about 90x-95x. Pytorch might be the next library which supports efficient parallelism across machines, but the library is not there yet. If you want to parallelize on one machine then your options are mainly CNTK, Torch, Pytorch. These library yield good speedups (3.6x-3.8x) and have predefined algorithms for parallelism on one machine across up to 4 GPUs. There are other libraries which support parallelism, but these are either slow (like TensorFlow with 2x-3x) or difficult to use for multiple GPUs (Theano) or both."

## Contribution guidelines ##

By adding material to this repository you certify that you have the right to add the material and that you want to make it publicly available under the [Creative Commons Attribution 4.0 International (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/) with attribution to Nordling Lab.

## Who do I talk to? ##

If you have any questions please email assistant@nordlinglab.org
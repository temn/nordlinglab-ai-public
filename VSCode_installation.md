**Installing VSCode with Docker extension (macOS, Windows and Ubuntu)**

*This brief tutorial is part of the [ documentation of Visual Studio Code ](https://code.visualstudio.com/docs) and written by Anette Kniberg, David Nokto and Alexander Heimann.*

The installation explained below depends on if you need also need Python. If you do not have/want Docker installed and therefore require Python installation, we recommend [installing VSCode as part of bundle with Anaconda Python](./AnacondaVSCode_installation.md). If you aim to use Docker and therefore not require a locally installed python, continue reading. 

___________________________________________
[TOC] 

____________________________________________

# Installing VSCode and Docker extension #


## Installation for macOS  

1. Download the latest stable version of VSCode by this [direct link for macOS](https://go.microsoft.com/fwlink/?LinkID=534106) 
2. Double-click on the downloaded archive to expand the contents.
3. Drag Visual Studio Code`.app` to the `Applications` folder, making it available in the `Launchpad`.
4. Add VSCode to your Dock by right-clicking on the icon and choosing `Options` > `Keep in Dock`.

This installation has been tested on OS X Yosemite. For a list of currently known issues, see Visual Studio Code Homepage [FAQ](https://code.visualstudio.com/docs/supporting/faq).

## Installation for Windows

1. Download the latest stable version of VSCode by this [direct link for Windows](https://aka.ms/win32-x64-user-stable)
2. Double-click and install

## Installation for Ubuntu

1. Install VS Code via "Ubuntu Software": Click the super button and type 'Ubuntu Software' to open it.
2. Search for Visual Studio Code and install it. 
3. Open VS Code afterwards by pressing the Super Key and typing Visual Studio Code or simply 'code'.
  

## Docker integration (macOS, Windows and Ubuntu)##

Docker support for VSCode is provided by extension. Once you have VSCode installed, open it and click on the `Extensions` section on the left most pane, and search for docker: 

![DockerExt_resized70.png](https://bitbucket.org/temn/nordlinglab-ai-public/downloads/2781155032-DockerExt_resized70.png)

Select the Microsoft Docker extension and install.

_______________________________________________________________

[Go back to Deep learning tutorial](https://bitbucket.org/temn/nordlinglab-ai-public/src/master/deep_learning_tutorial.md)
**Installing VSCode as part of bundle with Anaconda Python**  
*This brief tutorial uses parts of the [ documentation of Visual Studio Code ](https://code.visualstudio.com/docs) and the [documentation of Anaconda](https://docs.anaconda.com/anaconda/) and is written by Anette Kniberg, David Nokto and Alexander Heimann.*
 
-----------------------------------------------

[TOC]  

------------------------------------------------------

# Installing VSCode as part of bundle with Anaconda Python #

## Windows ##

1. Go to [this link](https://www.anaconda.com/download/#windows) and download the newest version of Anaconda
2. Double-click the installation file to start the installation
3. During the installation process, (tested under Windows 10) when asked if you want to "Add Anaconda to my PATH environment", IF and only IF you choose to use Anaconda Python alone, please check this option. If you want to use another Python Software, please read the warning and refer from checking the box.
4. Continue installing until you are offered to install VSCode and check this option to do so.


## macOS ##
Choose Anaconda installer depending on Python version:  

* [64-Bit Graphical installer for Python 3.7 version](https://repo.anaconda.com/archive/Anaconda3-5.3.0-MacOSX-x86_64.pkg) (recommended)
*  [64-Bit graphical installer for Python 2.7 version](https://repo.anaconda.com/archive/Anaconda2-5.3.0-MacOSX-x86_64.pkg)    

Alternatively, go to [this webpage](https://www.anaconda.com/download/#macos) for see more options.
Double-click on the .pkg file and follow the prompts on the installer screens. Accept all the defaults if you are unsure about any settings; They can be changed later. Near the end of the installation process there will be an option to install VS Code, as seen in the image below. NOTE: Installing VSCode with the Anaconda installer requires an internet connection. 

![vscode-macos-install35.png](https://bitbucket.org/temn/nordlinglab-ai-public/downloads/2300533883-vscode-macos-install35.png) 


##  Ubuntu 

1. Use [this link](https://www.anaconda.com/download/#linux) and download the newest version of Anaconda
2. Open a terminal (CTRL+T), navigate to the folder where you downloaded the file to, and type 

    `bash ~/Downloads/Anaconda3-5.3.0-Linux-x86_64.sh`

3. Press enter until you have to type yes, in order to proceed the installation process.
4. Press enter to confirm the installation folder.
5. Type yes when asked for initialization in that folder.
6. Type yes when asked if VS Code should be installed.
7. Check if Anaconda is installed correctly. Either open a terminal (CTRL+T) or open VS Code and within VS Code press CTRL+\` (the \` is on the left side above the "tab" key). Type the following code to see the Anaconda help:

    `anaconda -h`

If successful, you will see a list of commands for which you can get more information.

## Verifying your Anaconda installation and launch VSCode ##

After your install is complete, verify it by opening Anaconda Navigator, a program that is included with Anaconda. If Navigator opens, you have successfully installed Anaconda. If not then see [frequently asked questions](http://docs.anaconda.com/anaconda/user-guide/faq/) in  the Anaconda online documentation.

In Anaconda Navigator version 1.7 or higher, use the VS Code tile to launch VSCode.  
![vscode-tile-launch35.png](https://bitbucket.org/temn/nordlinglab-ai-public/downloads/2960307439-vscode-tile-launch35.png)

# Getting started with coding in Python #

When you install VSCode with Anaconda it comes with the [Python Extension for VSCode](https://marketplace.visualstudio.com/items?itemName=ms-python.python) for editing Python code.
The following instructions is a slimmed version of [this tutorial](https://code.visualstudio.com/docs/python/python-tutorial) to get started with Python on VSCode, where the three most important steps are:

1.**Selecting a Python Interpreter**   
 From within VS Code open the **Command Palette** (Windows: Ctrl⇧P, for macOS:  ⇧⌘P or F1), type **Python: Select 
 Interpreter** and select the command when it appears. A list of available Python interpreters will appear. Select 
 the desired one.

 ![commandpalette.png](https://bitbucket.org/temn/nordlinglab-ai-public/downloads/3469508844-commandpalette.png)
 ![pyinter.png](https://bitbucket.org/temn/nordlinglab-ai-public/downloads/2803359053-pyinter.png)
 

2.** Start project in a workspace folder**   
   Go to **File > Open Folder...** and then select the folder to use.

 ![openfolder.png](https://bitbucket.org/temn/nordlinglab-ai-public/downloads/3188885659-openfolder.png)

3.**Create .py-file**   
  Click on the "New File" button and give it a name ending with ".py". Start coding.

 ![toolbar-new-file.png](https://bitbucket.org/temn/nordlinglab-ai-public/downloads/237578921-toolbar-new-file.png)
 ![hello-py-file-created.png](https://bitbucket.org/temn/nordlinglab-ai-public/downloads/1797707364-hello-py-file-created.png)


## Installing a different version of Python with Anaconda  ##

This is a slimmed version of [this tutorial for managing python](https://conda.io/docs/user-guide/tasks/manage-python.html). 

To install a different version of Python without overwriting the current version, create a new environment and install the second Python version into it with Anaconda Python:

1. Create the new environment:

    * To create the new environment for Python 3.6, in your Terminal window, run:

        `conda create -n py36 python=3.6 anaconda`

        NOTE: Replace py36 with the name of the environment you want to create. Anaconda is the metapackage that includes all of the Python packages comprising the Anaconda distribution. python=3.6 is the package and version you want to install in this new environment. This could be any package, such as numpy=1.7, or multiple packages.

    * To create the new environment for Python 2.7, in your Terminal window, run:

        `conda create -n py27 python=2.7 anaconda`

2. Activate the new environment:
    * To see a list of all environments, use:
        `conda env list`

    * To activate one of them, use:
        `conda activate name`
      for the example above it would be
        `conda activate py36`

3. Verify that the new environment is your current environment. You should see the following if it is activated: `(myenv) $` or as in our example `(py36)`, 'myenv' simply stands for whatever name you chose. If you don't see that, do the following trick: to see the current environment use the `conda info --envs` command and to activate/deactivate showing the active environment in the command line use (true is for activating it, use false to deactivate it):`conda config --set changeps1 true`

4. To verify that the current environment uses the new Python version, in your Terminal window run:

    `python --version`

## Use pip within Anaconda ##

To use pip in your environment, in your Terminal window run:

    conda install -n myenv pip
    source activate myenv
    pip <pip_subcommand>

Here the [Conda Cheat Sheet](https://conda.io/docs/_downloads/conda-cheatsheet.pdf) for a more comprehensive list of commands.

## Tensorflow ##

In order to install a package and use it with Python, the pip command is used. Open a terminal and enter the following command in order to install Tensorflow:

    pip install tensorflow
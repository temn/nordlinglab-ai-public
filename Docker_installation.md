# **Docker Installation Guide** #
_________

* [Introduction](#markdown-header-introduction)  

* [Linux/Ubuntu](#markdown-header-linux)  
 
* [Mac OS](#markdown-header-mac-os)  

* [Windows](#markdown-header-windows)  


___________

## Introduction

Docker is a tool that solves environment settings and installation problems. It also makes it easier to package and run projects in other devices or systems.

If your project needs the software below to run, how will you set it up?
  

![icons50.png](https://bitbucket.org/temn/nordlinglab-ai-public/downloads/icons50.png) 
  

* **The traditional way:**
    You install all the software separately on your device. However, it is tedious to install, change settings and check versions. If you have many projects where some software tools are the same but based on different versions, it is troublesome to manage conflicts.
    
* **The Docker way:**  

    ![dockerWay50.png](https://bitbucket.org/temn/nordlinglab-ai-public/downloads/dockerWay50.png)

    If you use docker, it is easier to package your project to run in any system without having to worry about version conflicts.

**Note**:  

* Since almost all Docker documentation use the **command line** (no graphic interface), some basic knowledge about this is needed.  
  
  
* Command line syntax varies between different operation systems, so you should learn the one corresponding to your OS. If you use the Docker terminal, just use Linux's command line.  

---
## Linux
1. In the terminal, type the following command to install Docker:
    ```
    sudo apt-get install docker.io
    ```
2. After installation, you can check the status by this command:
    ```
    service docker status
    ```
    If it was successful, you will see the green message as in the image below:
    
    ![linuxDocker.png](https://bitbucket.org/temn/nordlinglab-ai-public/downloads/linuxDocker.png)
  

---


## Mac OS
*The following steps are based on this video:* https://www.youtube.com/watch?v=K90yOMr8wXo

1. Download the [latest stable version](https://download.docker.com/mac/stable/Docker.dmg)   
    
2. Double click docker.dmg. You will see this picture.
     
![docker-app-drag.png](https://bitbucket.org/temn/nordlinglab-ai-public/downloads/docker-app-drag.png)

3. Drag and drop the Docker icon (the whale icon) to the Applications folder, making it available in launchpad. 
   
4. Check if the installation was successful by checking which version is installed, with this command in the terminal:  
    ```Docker --version```
    
    ![mac_versioncall30.png](https://bitbucket.org/temn/nordlinglab-ai-public/downloads/mac_versioncall30.png)


---


## Windows 

Your system determines which Docker version to install. Both available versions require 64-bit operating system running Windows 7 or higher and at least 4GB RAM. Your Windows must also support Hardware Virtualization Technology and have virtualization enabled. 
  
**System requirements: Docker Desktop- Windows **  
 
Docker Desktop - Windows is Docker designed to run on Windows 10 and is the Community Edition (CE). It is a native Windows application and uses Windows-native Hyper-V virtualization and networking.
 
* **Windows 10 64bit: Pro or Enterprise.**  
* Virtualization is enabled in BIOS. 
* Hyper-V exists and is enabled.   
* CPU SLAT-capable feature.  
* At least 4GB of RAM.  

If you have an older version of Windows and do not have Hyper-V or choose not to enable it, consider Docker toolbox described below

**System requirements: Docker toolbox **  
  
* **64-bit operating system running Windows 7 or higher**  
* Virtualization is enabled in BIOS. 
* Hyper-V is disabled/ Do not have Hyper-V     
* At least 4GB of RAM.

If you know which system you have and how to handle virtualization, skip down to [3. Installation](#markdown-header-3-installation). Continue reading for instructions on how to check the requirements your system and how to enable virtualization.
  
### Check machine version (system requirements)

* #### Check the Windows operating system  
    
    Go to **Start** > type `Control Panel` > click `System`  

    ![checkingOSversion.png](https://bitbucket.org/temn/nordlinglab-ai-public/downloads/checkingOSversion.png)  

* #### Check if virtualization is disabled  

    Without virtualisation, Docker can not function on your machine. To check if your system supports virtualization and if its disabled or not, Please choose for which Windows you want to check: Windows 7 or Windows 8 or higher. If virtualization is disabled, be sure to later follow the instructions at **2. Set up your machine** for enabling it in BIOS.  
    
    **For windows 8 or higher**  

    Go to **Start** > type `Task Manager` > choose `Performance` tab > see `Virtualization` under **CPU** tab.  
 
    ![checkVirtNewer.png](https://bitbucket.org/temn/nordlinglab-ai-public/downloads/checkVirtNewer.png)

    **For windows 7**  
 
    To ensure if your processor supports virtualization/ has it enabled, [download this Hardware-Assisted Virtualization Detection Tool](https://www.microsoft.com/en-us/download/confirmation.aspx?id=592) from Microsoft which requires no installation. All you have to do is double-click the executable file. The image below shows displays the message for a processor without virtualization:

       ![checkingVirtWndws7.png](https://bitbucket.org/temn/nordlinglab-ai-public/downloads/checkingVirtWndws7.png)

     
### 2. Set up your machine  

* #### Enable Virtualization in BIOS (required for both Docker versions)  
   Remember /write down the instructions below before entering BIOS. 

    1. Open the BIOS by restarting your computer. Remember /write down the instructions 
    2. During boot process press *the key* (Common keys to enter the BIOS are F1, F2, F10, DEL, ESC) to enter BIOS before Windows starts.  
    * For laptop: If the booting does not provide the option to enter BIOS then try holding down the key (most often F2 or DEL) and click the power button. Do not release the key until you see the BIOS screen display.   
    3. Unfortunately, the menu in BIOS can vary for different types of motherboards, so you might need to search online for how to enter BIOS for your computer manufacturer. For windows 10: tap `Advanced Mode` > `Advanced settings` > `CPU Configuration` > find `Intel Virtualization Technology` > change `Disable` to **`Enable`** > `Save & EXIT`. 

* #### Checking Hyper-V (Hyper-V is needed for Docker Desktop, not for Docker Toolbox) 
 
    Hyper-V is a virtualization product from Microsoft and is needed to run Docker for Windows. Docker Toolbox on the other hand uses Oracle Virtual Box instead of Hyper-V. Both cannot be used simultaneously, so Hyper-V should in that case be disabled (if it exists).  

    Go to `Start` > type `Control Panel` > `Programs` > `Programs and Features` > `Turn Windows features on or off` (or you can directly type `Turn Windows features on or off` in the start search) > check/uncheck the box and click `OK` to enable/disable Hyper-V.   

    ![checkingHyperV.png](https://bitbucket.org/temn/nordlinglab-ai-public/downloads/checkingHyperV.png)    

    If you cannot find the Hyper V option among your directories, then you most likely do not have Hyper-V. It is possible to work around that, but we instead recommend that you use Docker Toolbox. Go to [installation for Docker Toolbox](#markdown-header-4-Docker Toolbox).
     
### 3. Installation

Now that you have ensured you have virtualization and checked which system you have, you are ready to choose which version to install. Again the requirements are presented. Note the difference between them regarding Hyper-V.

#### **Docker Desktop - Windows: system requirements**  

* **Windows 10 64bit: Pro or Enterprise.**  
* Virtualization is enabled in BIOS. 
* Hyper-V is enabled.     
* At least 4GB of RAM. 

    **Note**: If your system does not meet the requirements to run Docker for Windows, choose instead **Docker Toolbox**, which uses Oracle Virtual Box instead of Hyper-V.

1. [Download Docker for Windows installer](https://download.docker.com/win/stable/Docker%20for%20Windows%20Installer.exe) 
 
2. Double-click Docker for Windows Installer to run the installer.   

    When the installation finishes, Docker starts automatically. The whale in the notification area indicates that Docker is running, and accessible from a terminal. Settings are available on the UI, accessible from the Docker whale in the taskbar.

#### **Docker Toolbox: system requirements**  
  
* **64-bit operating system running Windows 7 or higher**  
* Virtualization is enabled in BIOS. 
* Hyper-V is disabled/ Do not have Hyper-V     
* At least 4GB of RAM.       

1. [Download DockerToolbox.exe for Windows](https://download.docker.com/win/stable/DockerToolbox.exe) and double- click it. Follow the instructions and include the following applications in the installation:  

    - Docker Client for Windows
    - Docker Toolbox management tool and ISO
    - Oracle VM VirtualBox
    - Git MSYS-git UNIX tools 
 
2. Double-click on Docker Quickstart icon in your desktop as below to run Docker Toolbox. 

    ![dockerQuickstart.png](https://bitbucket.org/temn/nordlinglab-ai-public/downloads/dockerQuickstart.png)  


### 4. Checking your installation
    
1. Type the command `docker --version` in the terminal / in the Docker Quickstart Terminal to check your version. 
    
2. Type the command `docker run hello-world`. The output should look like this:

    ![dockerTest2.png](https://bitbucket.org/temn/nordlinglab-ai-public/downloads/dockerTest2.png)
    

-----------------
[Go back to Deep learning tutorial](https://bitbucket.org/temn/nordlinglab-ai-public/src/master/deep_learning_tutorial.md)
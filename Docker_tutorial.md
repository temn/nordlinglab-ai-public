Docker tutorial
======

# Introduction

The *Dockerfile*, *image* and *container* are three important components of Docker. The Dockerfile is a text file that tells Docker how to set up the project and build the image. The image is an executable version of the application. After building the image you can run it to generate a container. Images and containers will persist on the computer even after it is shut down and will need to be removed manually. At the end of this tutorial, there is a list of useful Docker commands.
 
Here is the [**Dropbox link**](https://www.dropbox.com/sh/34e8oby24mortqy/AACWyl389Baf8kQLbTJHNMp_a?dl=0) to the Docker image-folder that you will be using for the deep learning tutorial. Download it (zip-file) and extract locally.

If you do not have a Windows computer, go [here](#markdown-header-running-local-scripts-through-docker) to learn how to use the downloaded Docker image.

To try some other Docker images, go [here](#markdown-header-additional-exercises).

## Preparations for Windows users ##

### For Docker Desktop ###
Make sure Shared Drives are activated in Settings. The chosen drives will be available for Docker.

![DockerSettings.png](https://bitbucket.org/temn/nordlinglab-ai-public/downloads/3129692236-DockerSettings.png)

![DockerSharedDrives.png](https://bitbucket.org/temn/nordlinglab-ai-public/downloads/4059783799-DockerSharedDrives.png)

### For Docker Toolbox ###
Make sure that Shared Drives are activated in the settings of Oracle VirtualBox. If you do not have a folder under "Machine Folders", choose a folder and make sure to active "Auto Mount" and "Make Permanent". The chosen folder and all sub-folders inside it will be available for Docker. 

![oracle.png](https://bitbucket.org/temn/nordlinglab-ai-public/downloads/565304073-oracle.png)

![oraclesettings.png](https://bitbucket.org/temn/nordlinglab-ai-public/downloads/1078816338-oraclesettings.png)

![oraclesettings2.png](https://bitbucket.org/temn/nordlinglab-ai-public/downloads/oraclesettings2.png)

![oraclesettings3.png](https://bitbucket.org/temn/nordlinglab-ai-public/downloads/oraclesettings3.png)

![oraclesettings4.png](https://bitbucket.org/temn/nordlinglab-ai-public/downloads/oraclesettings4.png)

# Running local scripts through Docker #
## Alternative 1: Docker Volumes ##
Make sure the working folder is the one where the Dockerfile is. To build an image called *aitut*, type this in the terminal:
```
docker build --rm -f "Dockerfile" -t aitut .
```
To make changes in python scripts locally without rebuilding the Docker image, use the following command line:
```
docker run -it -v <absolute path of your local folder>:<container folder> <image id or name>
```
This will open a connection (Docker Volume) between the two folders while the container is running.

### Example ###

We have a script, playground.py, in a folder called docker_tf_intro/index. In the container, the script is put in the folder /app/index. Therefore we use this command:
```
docker run -it -v /C/Users/nordlinglab-ai-public/docker_tf_intro/index:/app/index aitut
```
![runningcontainer.png](https://bitbucket.org/temn/nordlinglab-ai-public/downloads/2503263632-runningcontainer.png)

**Note:** for Windows with Docker Toolbox, remember to write **//c/** (lowercase) instead of **/C/**, also note the direction of the slashes ("/"). For macOS use tilde ("~") to refer to the Home Directory.

We are now inside the container, in the folder /app/index. In order to run playground.py we use this command:
```
python playground.py
```

![runningplaygroundinvolumes.png](https://bitbucket.org/temn/nordlinglab-ai-public/downloads/3566044716-runningplaygroundinvolumes.png)

Now lets make a change to playground.py locally (and save it) while the container is still running.

![newprintstatementvolumes.png](https://bitbucket.org/temn/nordlinglab-ai-public/downloads/3406554683-newprintstatementvolumes.png)

Now we can simply type **python playground.py** again (or press the Up arrow) to run the updated script.


![runningplaygroundinvolumes2.png](https://bitbucket.org/temn/nordlinglab-ai-public/downloads/3264684132-runningplaygroundinvolumes2.png)

When we are done with the container, we can exit by typing **exit**. It is also good to occasionally remove unused copies of images, containers and volumes with this command:
```
docker system prune --volumes -f
```
---
## Alternative 2: Rebuilding images ##
If for some reason Alternative 1 does not work on your computer, use this method. 
Make sure the working folder is the one where the Dockerfile is. In the Dockerfile, make sure to have the command CMD python3 playground.py uncommented and save it. This will make the container run the python script called "playground.py" as soon as the container is running. 

![uncomment.png](https://bitbucket.org/temn/nordlinglab-ai-public/downloads/137147321-uncomment.png)

Build an image called *aitut*:
```
docker build --rm -f "Dockerfile" -t aitut .
```
![aitutbuild.png](https://bitbucket.org/temn/nordlinglab-ai-public/downloads/3597252774-aitutbuild.png)

Run the container:
```
docker run -it aitut
```
![aitutrun.png](https://bitbucket.org/temn/nordlinglab-ai-public/downloads/3019811923-aitutrun.png)

It is also good to occasionally remove unused copies of images and containers:
```
docker system prune --volumes -f
```
![aitutprune.png](https://bitbucket.org/temn/nordlinglab-ai-public/downloads/565550319-aitutprune.png)

Now lets make some changes to playground.py (and save it):

![changed python script.png](https://bitbucket.org/temn/nordlinglab-ai-public/downloads/3631382835-changed%20python%20script.png)

In order for us to run the updated script, the image will need to be rebuilt before the container is run. It is therefore suggested to use the following command to speed it up (if you're using Windows Powershell, replace "&&" with ";"):
```
docker build --rm -f "Dockerfile" -t aitut . && docker run -it --rm aitut && docker system prune --volumes -f
```
This will build the image, run the container (and python script) and prune the system. After saving the next change in playground.py, simply paste this in the terminal (or press the Up arrow) to run the script. In this case the output will look like:

![newoutput.png](https://bitbucket.org/temn/nordlinglab-ai-public/downloads/3146879841-newoutput.png)

---

---

# Additional Exercises #

## Neural network ##

[Here](https://www.dropbox.com/sh/9elkdk4w8jrlw5j/AADavAIbQnJGP_8h4wYk3waOa?dl=0) is a Dropbox link to a simple neural network example project for Docker. To run the project, download the folder and follow the steps in the Readme-file.


## Web project ##

You can also use Docker to set up a webserver. Here we create an image through a file called *Dockerfile*, and use a simple Python code that can run a basic web server.

### 1. Get the project ###

The project is made available through this [Dropbox link](https://www.dropbox.com/sh/498ic8ni2c0ekoe/AACL_wR_9CSeMBbNZ6gLX_MCa?dl=0).

### 2. Write a Docker file ###
First, enter the example folder
```
cd example
```
Make a new docker file called *Dockerfile* and edit it.

You can choose use any code editor, like VSCode or Sublime to edit this file (recommended) or use the command line by entering:
```
nano Dockerfile
```


and edit these settings in the *Dockerfile*
```
FROM node:latest  
WORKDIR /app 
ADD . /app/ 

RUN npm install 

EXPOSE 3000
CMD [ "npm", "run", "start" ] 
```
Here is a description of the code above:  

```FROM node:latest``` Based on nodejs image from Dockerhub
```WORKDIR /app``` Create a working folder when starting the container
```ADD . /app/``` Add all the files from current folder to the working folder in the container
```RUN npm install``` Install all the needed packages based on package.json file
```EXPOSE 3000``` The exposure port from container
```CMD [ "npm", "run", "start" ]``` The command for starting the container

We can now build the image file:
```
docker build -t example-image .
```
with ```example-image``` as the chosen name of your image file. 

After building up the image, you can check the image status by
```
docker images
```
![](https://bitbucket.org/temn/nordlinglab-ai-public/downloads/kQOwo7V.png)

If you see the image name, it is done.

### 3. Start the container
Remember, the container only exists when we run the image.
We can start it by the following command
```
docker run -p 5487:3000 --name example-container example-image
```
```-p 5487:300``` means ```<external port>:<container port>```
```--name example-container``` is container name
```example-image``` is the image name that you want to run

![](https://bitbucket.org/temn/nordlinglab-ai-public/downloads/1c2UQnC.png)

After that, we have started the container and run the server.

If we want to connect to this server in browser, we need to know our local ip, which we retrieve using the following command:
```
docker-machine ip
```


Type the ip address with port 5487 in our browser
```
<your docker-machine ip>:5487
```
It will show as below:

![](https://bitbucket.org/temn/nordlinglab-ai-public/downloads/y741OBv.png)



If we want to check the container status by showing which containers are running:
```
docker ps
```


---



# Helpful commands

* Restart the stopped container
```
docker restart <stopped container name>
```
* Remove the stopped container
```
docker remove <stopped container name>
```
* Stop the running container
```
docker stop <running container name>
```
* Enter the running container environment
```
docker exec -it <container-id> bash
```
or
```
docker exec -it <container-id or name> /bin/bash
```
* List all existing containers
```
docker ps -a
```
* List all existing images
```
docker images
```
* Start the container and enter its environment
```
docker run -it <image id or name> /bin/bash
```
* Folder binding between container and your local
```
docker run -it -v <absolute path of your local folder>:<container folder> <image id or name> /bin/bash
```
 Example
 ```
 docker run -it -v ~demo:/demo demo /bin/bash
 ```

---

[Go back to Deep learning tutorial](./deep_learning_tutorial.md)